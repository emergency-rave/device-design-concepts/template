![Sketch Design with PC fans](images/basicDesignPcFans.png)

# BOL

| Item | estimated retail cost |
| ---- | --------------------- |
| Raspberry Pi 4 (or similar single board computer) | EUR 40,00 |
| Raspberry Pi Zero WH with pre-soldered header (or similar) | EUR 30,00 |
| 2 MicroSD cards 32 GB | EUR 15,00 |
| Pimoroni Enviro pHAT or similar sensor module | EUR 20,00 |
| small grid style circuit board (e.g. 4x6cm)  | EUR 0,50 |
| 5-10 male-female jumper wire  | EUR 2,00 |
| 5-10 female-female jumper wire | EUR 2,00 |
| 8 channel relay module 5V  | EUR 10,00 |
| 2-4 80 mm PC fans, clockwise rotation, optimized for static pressure, ca. 3000 RPM  | EUR 30,00 |
| 2-4 80 mm PC fans, counterclockwise rotation  | EUR 30,00 |
| 4-8 resistors 470 Ω | EUR 0,01 |
| USB power bank with pass through function and at least 2 outputs | EUR 35,00 |
| Used Android Smartphone | -- |
| 2 empty PET plastic bottles (one soft, one more rigid) | -- |
| Used car battery | -- |
| 1-2m silicon or pvc tube, ca. 9mm wide | EUR 5,00 |
