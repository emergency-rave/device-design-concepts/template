# About this template project

Please use this GitLab project as a template for you own design documentation.

Your design should roughly comply to the following file structure:

* README.md: Description of the basic concepts of your design, like below.  
* BOL.md: "Shopping List" to build your design. If possible, please be rather **unspecific**, because parts that are easy to get in your part of the world might be scarce elsewhere. If they can be replaced by similar parts, please point this out.
* Assembly.md: Assembly manual for your design. Again please keep it **unspecific** whenever things can be done in multiple ways.
* Testing.md: Testing manual for your design. This document should contain a collection of function- and security tests that can be used to make sure, that your design has been properly implemented.
* Warnings.md: Experts are encouraged to scrutinize designs and warn you of potential flaws and dangers using Gitlab issues. If you receive such a warning (and if it is justified) and you can not solve it immediately, please document it here.   
* images/: folder for images in you documentation. Please dont add videos or other unusually large files here. Instead please publish them on Dropbox, Youtube, etc. and provide a link in the README.md file.  
* doc/: folder for posters, powerpoints, papers, etc. that you would like to publish together with your design (if any). If you have published them elsewhere, please provide a link in the README.md. 
* models/: folder for 2D/3D CAD models, cutting masks and input files for 3D printers

# Basic Design Concept

Here we sketch out a design that is easy to assemble from widely available materials.

![Sketch Design with PC fans](images/basicDesignPcFans.png)

The retail price of the materials (see document BOL.md) is roughly 200 Euro. If purchased in bulk or recycling parts (e.g. from old PCs), the price can be even much lower than this.  

## Pressure Source

The pressure source is build as a stack of 8cm PC cooling fans, that can be taken out of old Desktop PCs. To increase the air throughput we alternate clockwise and counterclockwise rotating fans, but this is not a requirement. Counterclockwise rotating fans might be difficult to acquire. If the air throughput is not high enough, this can be solved by more fans.

If available, fans that are optimized for static pressure are preferrable, but not required.

We use a soft PET plastic bottle with the bottom cut out as a turbine case around the fan stack. 

## Sensor Unit

We use a second, harder (reusable) plastic bottle as pressure chamber housing sensors for pressure (required), temperature (optional), humidity (optional) and particles (optional).

For our prototype we have chosen a Raspberry Pi nano with a *Pimoroni EnviroPhat* as a sensor unit, which communicates to the control unit via Wifi. 

![Sensor Unit](images/enviroPhat.png)

Alternatively a simple sensor connected to the control unit via cable would be possible too, of course. In fact, that would be an improvement, and we urge the community to pick up this idea and work out the details.

## Control Unit

The control unit is responsible for triggering the motion of the fans depending on the phase of the breath cycle and the pressure measured by the sensor unit. It also communicates with the user interface app, sending measurement data, alarm signals on unexpected events, and receiving the parameters of the breath cycle. 

We have built the control unit using a Raspberry Pi 3 connected to a relais module to switch the power supply to the fans.

## User Interface

The user interface is realized as an Android app. The Android phone can also serve as a Wifi router, if Wifi is not available otherwise.

## Power Sources

We need two power sources: 
* 12V for the fan stack
* 5V (USB) for the Raspberry Pi's and for the Smartphone.

To prevent outages during power grid failures the 12V should be provided by a car battery, which is connected to a battery charger to keep it loaded while the grid is operational.

For the same reason, the 5V power should be backed up by a retail USB power bank. 

## Safety of use
See document Warnings.md.
> **Important**: The device proposed in this project is not meant to be a cheap alternative to regular medical equipment. Neither the design nor the materials come close to the quality standards of professional devices approved by regulators. Any device built after the design proposed here is probably unreliable and might be dagerous to the patient. **THEREFOR IT IS ONLY TO BE USED, WHEN THE ALTERNATIVE IS THE CERTAIN DEATH OF THE PATIENT**. The patient as well as the function of the device must be closely monitored. A device built after the design proposed here must not be used if a better alternative is available.  

## Status

Not ready: **The proposed design has not been tested yet. The software is still under development.**



## Legal notice

The proposed design is subject to the CERN Open Hardware Licence Version 2 (see document LICENSE). 
If you implement the design proposed here or use a device built after this design, you agree, that the designer as well as the builder are not liable for any damage caused by the construction or use of the device including loss of life. You understand that the device may only be used when the alternative to using it is the certain death of a patient as assessed by a medical professional. **If this total exclusion of liability is not legally possible in your country, you must not use this design.**
 

# Reference
* Sensor documentation: http://docs.pimoroni.com/envirophat/

* Sensor tutorial: https://learn.pimoroni.com/tutorial/sandyj/getting-started-with-enviro-phat