# Warnings

> **Important**: The device proposed in this project is not meant to be a cheap alternative to regular medical equipment. Neither the design nor the materials come close to the quality standards of professional devices approved by regulators. Any device built after the design proposed here is probably unreliable and might be dagerous to the patient. **THEREFOR IT IS ONLY TO BE USED, WHEN THE ALTERNATIVE IS THE CERTAIN DEATH OF THE PATIENT**. The patient as well as the function of the device must be closely monitored. A device built after the design proposed here must not be used if a better alternative is available.  

